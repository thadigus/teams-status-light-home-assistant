# Teams Status Light Home Assistant

This is a simple Python application that will troll your local Microsoft Team's status logs and perform necessary API requests to Home Assistant to update an RGB capable light. This app features an YAML based configuration format. While this repo was created by [Thad Turner](https://gitlab.com/thadigus) I would like to shout out [lorentzfactr](https://github.com/lorentzfactr) for their work to create the Teams log interface.

# Getting Started - Basic Windows Install

Ensure that Python 3 and Pip is installed.

Clone down the git repo to desired path on the OS. This will have to sit here long term so don't use your Downloads folder.

`git clone git@gitlab.com:thadigus/teams-status-light-home-assistant.git`

Configure the Python application.

`cd teams-status-light-home-assistant`

`pip install -r requirements.txt`

`cp example-config.yml config.yml`

`notepad.exe config.yml`

Edit the configuration for your Home Assistant instance, device, desired colors, and Teams AppData location (change username in path). For more information see [sample configuration section](https://gitlab.com/thadigus/teams-status-light-home-assistant#sample-configuration).

Once the configuration has been created you can test by running `main.py`. If the application is working properly you should create a shortcut to the `main.py` application and drop it in your `C:\Users\username\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup` folder to run on startup. Right click and edit the properties of your shortcut. Ensure that the `Start in:` field is the directory of the repo on the system, and ensure that the `Taget:` is set to use pythonw.exe like this: `C:\Users\username\AppData\Local\Programs\Python\Python310\pythonw.exe C:\Users\username\teams-status-light-home-assistant\main.py`. This should create the necessary shortcut to run on startup once placed in the startup folder.

# Sample Configuration

```yaml
# URL base to Home Assistant instance (DO NOT ADD TRAILING /)
ha_url: "http://10.0.1.10:8123"
# Create a long-lived access token in Home Assistant UI by clicking your name in the bottom left and scrolling down to 'Long Lived Access Tokens'
ha_token: "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()"
# Entity ID of light you wish to control with Home Assistant, found by navigating to the light's control, clicking on the gear, and copying the entity ID.
entity_id: "light.name"
# RGB Values of different Teams status colors.
custom_return:
  - "Green": "0,255,0"
  - "Yellow": "255,255,0"
  - "Red": "255,0,0"
  - "Off": "255,136,13"
# File path to folder that contains Team's AppData, make sure logs.txt is present.
filepath: "C:\\Users\\username\\AppData\\Roaming\\Microsoft\\Teams"
```

# Known Bugs

At this time the application will run on startup but it does not turn off the light. If you're lucky it will go to your offline state while the computer is shutting down but it is not likely. For me it's not that big of a deal to flip the switch so I will develop a solution for this in the future. Hopefully there is an easy way to detect the application being asked to gracefully shutdown and performing the off action then. Please create an issue/MR if you are able to do this.

# Further Development

Most development is going to be track with the [issues](https://gitlab.com/thadigus/teams-status-light-home-assistant/-/issues) page, and all features and bug fix requests can be submitted there for future work. If you see an issue you can easily solve, please feel free to open an MR!

# Contributors

I wanted to create my own project for this, not because it did not exist, but because I wanted some practice and developing and maintaining my own code. This code is highly borrowed from the following repo, thank you very much [lorentzfactr](https://github.com/lorentzfactr)!

<https://github.com/lorentzfactr/teams_status/tree/main>

<https://turnerservices.cloud>