# A large portion of this was adapted from https://github.com/lorentzfactr/teams_status/blob/main/teams_light.py !!!
#
# Import custom Teams log Trolling Library, requests, and time.
from teams_status import teamsStatus
from requests import get,post
from time import sleep,time
import yaml

# Load in config from config.yml
def load_config_params(filepath):
    with open(str(filepath), 'r') as file:
        config_params = yaml.safe_load(file)
    return config_params
config = load_config_params("config.yml")
print(config["custom_return"]["Green"])

# Temp var to store that last status reported.
last_status = None

# Variable for the hard check timer. You can update the interval (seconds) to check status every 'interval' seconds.
# If you don't want a hard interval check, set to very large value.
start = 0
interval = 10 #UPDATE if desired

# Instatiate the teams status class, leave kwargs empty for default settings. The logic instatiates the class based on
teams = teamsStatus(green_hue=config["custom_return"]["Green"],
                    yellow_hue=config["custom_return"]["Yellow"],
                    red_hue=config["custom_return"]["Red"],
                    off_hue=config["custom_return"]["Off"],
                    filepath=config["filepath"]
                    )

url = config["ha_url"] + "/api/"
headers = {
    "Authorization": "Bearer " + config["ha_token"],
    "content-type": "application/json",
}
response = get(url, headers=headers)
print(response.text)
if str(response.text) != '{"message":"API running."}':
    exit()

# A forever while loop since this is designed to run on start up and not shut down.
while True:
    # Get the current Teams status.
    status = teams.get_status()
    # Get the RGB color from the current Teams status.
    color, color_name = teams.status_color(status)
    # If different, send update to Home Assistant
    if status != last_status or interval <= time()-start:
        instruction = f'{color}'
        print(f'Current Teams Status: {status} | Status Color: {color_name} | RGB {instruction}')
        url = config["ha_url"] + "/api/services/light/turn_on"
        headers = {
            "Authorization": "Bearer " + config["ha_token"],
            "content-type": "application/json",
        }
        data = { "entity_id": config["entity_id"],
                "rgb_color": instruction.split(",")
                }
        response = post(url, headers=headers, json=data)
        print(response.text)
        start = time()
    # Do nothing if the status hasn't changed or interval requirement has not been met.
    else:
        pass
    # Sleep for one second to reduce the amount of log file reads (in seconds).
    sleep(1)
    # Store the last known status change in the temp variable.
    last_status = status